{{-- we are extending the navbar --}}
@extends('layouts.app')

@section('content')

{{-- to install the snippet for laravel blade: ctrl shift p -> package control: install package -> laravel blade highlighter --}}
	{{-- csrf cross site request forgery --}}
	{{-- yung tempered requests will not be processed --}}
	{{-- pag walang csrf, magkakaroon tayo ng err --}}
	{{-- we add this to our form para mas secured --}}
	<form method="POST" action="/posts">
		@csrf
		
		<div class="form-group">
			<label for="title">Title:</label>
			<input type="text" class="form-control" id="title" name="title">
		</div>

		<div class="form-group">
			<label for="content">Content:</label>
			<textarea class="form-control" name="content" id="content" rows="3"></textarea>
		</div>

		<div class="mt-2">
			<button type="submit" class="btn btn-primary">Create Post</button>
		</div>
		
	</form>
@endsection